# Jan-Philipp Kolb
# Wed Jun 20 22:02:18 2018
# tram stops for Mannheim


library("osmdata")

dat3 <- opq(bbox = 'Mannheim') %>%
  add_osm_feature(key = 'railway', 
                  value = 'tram_stop') %>%
  osmdata_sf ()

setwd("D:/gitlab/rspatial/slides/")

save(dat3,file="../data/osmdata_tram_mannheim.RData")
